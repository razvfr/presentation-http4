# Presentation Http4K

## Structure
 - App: src/main -> `Http4kTalk.kt` permet de lancer le site pour la presentation
 - Test presentation: src/test -> `Presentation.kt` code des slides
 - resources: 
    - slides/ -> correspond à `/` les slides
    - swaggger/ -> correspond à `/swagger` swagger ui
    - terminal/ -> client js pour le terminal websockets (à utiliser à vos risques) 

## Lancer le serveur
 - Lancer le `main` de `Http4kTalk.kt`
 - aller a `http://localhost/` pour les slides
    - `/swagger` pour le swagger
    - `/terminal` pour le terminal
