package fr.rzv

import com.natpryce.hamkrest.and
import com.natpryce.hamkrest.assertion.assertThat
import io.kotest.assertions.shouldFail
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.equality.shouldBeEqualToIgnoringFields
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNot
import io.kotest.matchers.string.beEmpty
import org.http4k.client.OkHttp
import org.http4k.core.*
import org.http4k.core.Status.Companion.OK
import org.http4k.filter.ClientFilters
import org.http4k.filter.ServerFilters
import org.http4k.format.Jackson.auto
import org.http4k.hamkrest.hasBody
import org.http4k.hamkrest.hasStatus
import org.http4k.lens.*
import org.http4k.routing.bind
import org.http4k.routing.path
import org.http4k.routing.routes
import org.http4k.server.Http4kServer
import org.http4k.server.Undertow
import org.http4k.server.asServer
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.LocalTime

class Presentation {

    @Test
    fun `Concept - HttpMessage`() {
        /**
         * ## Un message sérialisé au standard HTTP
         */

        val request = Request(Method.GET, "http://server/path")
            .query("name", "value")
            .header("name", "value")
            .body("Hello world")

        val response = Response(Status.I_M_A_TEAPOT)
            .header("name", "value")
            .body("Hello world")

        request shouldBe response
    }


    @Test
    fun `Concept - HttpHandler`() {
        /**
         * ## Transforme une requête HTTP en une réponse HTTP
         *
         * `typealias HttpHandler = (Request) -> Response`
         *
         */

        val echo: HttpHandler = { req: Request ->
            Response(OK).body(req.body)
        }

        val resp: Response = echo(Request(Method.POST, "/echo").body("hello"))
        println(resp)
        resp.bodyString() shouldBe "hello"
    }


    @Test
    fun `Concept - Filter`() {
        /**
         * ## Effectue un traitement avant ou après le traitement sur une operation HTTP
         *
         * `Filter: (HttpHandler) -> HttpHandler`
         */

        val echo: HttpHandler = { req: Request ->
            Response(OK).body(req.body)
        }
        val twitterFilter: Filter = Filter { next: HttpHandler ->
            { req: Request ->
                val tweet: Request = req.body(req.bodyString().take(280))
                next(tweet)
            }
        }
        val tweet: HttpHandler = twitterFilter.then(echo)

        val respEcho: Response = echo(Request(Method.POST, "/echo").body("hello".repeat(300)))
        val respTweet: Response = tweet(Request(Method.POST, "/echo").body("hello".repeat(300)))
        println(respTweet)
        respTweet.bodyString().length shouldBe 280
        respEcho.bodyString().length shouldBe "hello".length * 300
    }


    @Test
    fun `Concept - Router`() {
        /**
         * ## Fait correspondre un HttpHandler à une Requête
         *
         * `Router: (Request) -> HttpHandler?`
         */

        val echo: HttpHandler = { req: Request ->
            Response(OK).body(req.body)
        }
        val twitterFilter: Filter = Filter { next: HttpHandler ->
            { req: Request ->
                val tweet: Request = req.body(req.bodyString().take(280))
                next(tweet)
            }
        }
        val tweet: HttpHandler = twitterFilter.then(echo)
        val routes: HttpHandler = routes(
            "echo" bind Method.POST to echo,
            "twitter" bind routes(
                "tweet" bind Method.POST to tweet
            )
        )
        val respEcho: Response = routes(Request(Method.POST, "/echo").body("hello".repeat(300)))
        val respTweet: Response = routes(Request(Method.POST, "/twitter/tweet").body("hello".repeat(300)))
        val respNotFound: Response = routes(Request(Method.GET, "/twitter"))

        respNotFound.status shouldBe Status.NOT_FOUND
        respEcho.bodyString().length shouldBe "hello".length * 300
        respTweet.bodyString().length shouldBe 280
    }


    @Test
    fun `Tests des HttpHandler`() {
        /**
         *  ## Tests faciles car les messages sont des `data class`
         */

        val echo: HttpHandler = { req: Request ->
            Response(OK).body(req.body)
        }
        val input: Request = Request(Method.POST, "/echo").body("hello")
        val expected: Response = Response(OK).body("hello")

        echo(input) shouldBe expected
    }


    @Test
    fun `Functions sur le reseau`() {
        /**
         * ## On attache le HttpHandler à un serveur
         *
         * Support de:
         * ```
         *    Jetty | Netty  | Undertow
         *    Ktor  | Apache | Sun HttpServer
         * ```
         */

        val echo: HttpHandler = { r: Request -> Response(OK).body(r.body) }
        val server: Http4kServer = echo.asServer(Undertow(8081)).start()

        // decommenter la ligne qui suit pour blocker le serveur ( qu'il ne se ferme pas a la fin de la fonction)
        // pour arreter Ctrl-C ou Stop dans l'IDE, puis recommanter la ligne pour pas bloquer si vous lancez tout
        // server.block()
        // > http GET :8081/test body='Bonjour !'
    }


    @Test
    fun `Client HTTP`() {
        /**
         * ## Fait correspondre une requête HTTP à une Réponse
         *
         * `HttpHandler: (Request) -> Response`
         *
         * Support de:
         * ```
         *    Apache | OkHttp | Jetty | Java
         * ```
         */

        val echo: HttpHandler = { r: Request -> Response(OK).body(r.body) }
        val server: Http4kServer = echo.asServer(Undertow(8081)).start()

        val client: HttpHandler = OkHttp()
        val response: Response =
            client(Request(Method.POST, "http://localhost:8081").body("Bonjour"))

        response.bodyString() shouldBe "Bonjour"
    }


    @Test
    fun `Contrats HTTP`() {
        /**
         * ## Renforcer les contrats des requêtes entrantes:
         *  - Localisation : **Path** | **Query** | **Header** | **Body** | **Form**
         *  - Obligatoire ou optionnel
         *  - Traitement du body avec vérification des types
         * ## Et des réponse !
         */

        val miner: HttpHandler = routes(
            "mine/{btc}" bind Method.POST to { r: Request ->
                val newTotal: Int = r.path("btc")!!.toInt() + 1
                val out = "Vous avez $newTotal BTC"
                val json = """{ "value": "$out" }"""
                Response(OK).body(json)
            }
        )

        val responseOk = miner(Request(Method.POST, "mine/2"))
        responseOk.bodyString() shouldBe """{ "value": "Vous avez 3 BTC" }"""

        shouldThrow<NumberFormatException> {
            miner(Request(Method.POST, "mine/deux"))
        }
    }

    @Test
    fun `Concept - Lens`() {
        /**
         * ## Un Lens (lentille) cible une partie en particulier d'un objet complexe afin de récupérer ou définir une valeur
         *
         * `Extract: (HttpMessage) -> X`
         *
         * `Inject: (X, HttpMessage) -> HttpMessage`
         */

        val btcPath = Path.int().of("btc")

        val miner: HttpHandler = routes(
            "mine/{btc}" bind Method.POST to { r: Request ->
                val newTotal: Int = btcPath.extract(r) + 1
                val out = "Vous avez $newTotal BTC"
                val json = """{ "value": "$out" }"""
                Response(OK).body(json)
            }
        )

        val responseOk = miner(Request(Method.POST, "mine/2"))
        responseOk.bodyString() shouldBe """{ "value": "Vous avez 3 BTC" }"""

        shouldThrow<LensFailure> {
            miner(Request(Method.POST, "mine/deux"))
        }
    }


    data class BTC(val value: Int) {
        operator fun plus(that: BTC) = BTC(value + that.value)
        override fun toString() = value.toString()
    }

    class MindedCoin(btc: BTC) {
        val value = "Vous avez ${btc + BTC(1)} BTC"
    }

    @Test
    fun `Exemple - Lens`() {
        // Mapping avec type du domaine
        val btcPath = Path.int().map(::BTC).of("btc")

        // Sérialisation automatique en JSON avec Jackson (marche aussi avec Gson et Moshi)
        val jsonBody = Body.auto<MindedCoin>().toLens()

        val miner: HttpHandler = routes(
            "mine/{btc}" bind Method.POST to { r: Request ->
                val mined = MindedCoin(btcPath.extract(r))
                val response = jsonBody.inject(mined, Response(OK))
                println(response.bodyString())
                response
            }
        )

        // val responseOk = miner(Request(Method.POST, "mine/2"))
        val responseOk = miner(Request(Method.POST, "mine/2"))
        responseOk.bodyString() shouldBe """{"value":"Vous avez 3 BTC"}"""

        shouldThrow<LensFailure> {
            miner(Request(Method.POST, "mine/deux"))
        }
    }

    @Test
    fun `Applications stacks`() {
        val btcPath = Path.int().map(::BTC).of("btc")
        val jsonBody = Body.auto<MindedCoin>().toLens()

        val miner: HttpHandler = routes(
            "mine/{btc}" bind Method.POST to { r: Request ->
                val mined = MindedCoin(btcPath.extract(r))
                val response = jsonBody.inject(mined, Response(OK))
                response
            }
        )

        val logFilter = Filter { next: HttpHandler ->
            { r ->
                println("${LocalTime.now()} LOG: START: ${r.method}:${r.uri}")
                val resp = next(r)
                println("${LocalTime.now()} LOG: END: ${resp.status}")
                resp
            }
        }
        val appWithLogs = logFilter.then(miner)

        appWithLogs(Request(Method.POST, "mine/2"))
        miner(Request(Method.POST, "mine/2"))

        val contexts = RequestContexts()
        val credentials = RequestContextKey.required<Credentials>(contexts)
        val securedApp = ServerFilters.InitialiseRequestContext(contexts)
            .then(ServerFilters.BearerAuth(credentials) {
                if (it == "OK") Credentials("user", "password") else null
            })
            .then(appWithLogs)

        println(securedApp(Request(Method.POST, "mine/2")))
        println(
            securedApp(
                Request(Method.POST, "mine/3")
                    .header(
                        "Authorization", "Bearer OK"
                    )
            )
        )
    }
}

abstract class TestContract {
    open val app : HttpHandler = { req: Request ->
        Response(OK).body(req.body)
    }

    @Test
    fun `response body should be same a request`() {
        val expected: Response = Response(OK).body("hello")
        val input: Request = Request(Method.POST, "/echo").body("hello")
        assertThat(app(input), hasStatus(expected.status) and hasBody(expected.bodyString()))
    }
}

class TestInMemory : TestContract() {
    override val app: HttpHandler = { req: Request ->
        Response(OK).body(req.body)
    }
}
class TestAsServer : TestContract() {
    private val server = { req: Request ->
        Response(OK).body(req.body)
    }.asServer(Undertow(0)).start()

    override val app: HttpHandler = ClientFilters
        .SetBaseUriFrom(Uri.of("http://localhost:${server.port()}"))
        .then(OkHttp())
}


data class Joke(val value: String)
class ChuckNorris(private val client: HttpHandler) {
    private val jsonBody = Body.auto<Joke>().toLens()
    fun randomJoke(): Joke {
        val response = client(Request(Method.GET, "https://api.chucknorris.io/jokes/random"))
        return jsonBody.extract(response)
    }
}

abstract class ChuckNorrisCustomerContract {
    abstract val client: HttpHandler

    @Test
    fun getJoke() {
        val chuck = ChuckNorris(client)
        val joke = chuck.randomJoke()
        joke.value shouldNot beEmpty()
    }
}

class RealChuckTest : ChuckNorrisCustomerContract() {
    override val client: HttpHandler = OkHttp()
}

object FakeChuckServer {
    private val jsonBody = Body.auto<Joke>().toLens()
    private val testHeader = Header.defaulted("x-test", "")
    operator fun invoke(): HttpHandler = { req: Request ->
        when (testHeader(req)) {
            "teapot" -> Response(Status.I_M_A_TEAPOT)
            else -> jsonBody.inject(Joke("fake joke"), Response(Status.OK))
        }
    }
}

class FakeChuckTest : ChuckNorrisCustomerContract() {
    override val client: HttpHandler = FakeChuckServer()
    private val headerChangeFilter = Filter { next: HttpHandler ->
        { request ->
            val newRequest = request.header("x-test", "teapot")
            next(newRequest)
        }
    }

    @Test
    fun teapotTest() {
        val chuck = ChuckNorris(headerChangeFilter.then(client))
        // Si on enleve le commentaire le test va echouer, il va montrer une erreur de conception
        // qui suppose que tous les reponses ont un body
        // val joke = chuck.randomJoke()
    }
}

