# Serveur comme fonction en Kotlin


<div style="text-align: center; border: 0; background: None; box-shadow: None;" class="fragment" data-fragment-index="1">
    <img src="img/http4k-bk.png" alt="logo" style="border: 0; background: None; box-shadow: None">
</div>

Razvan<!-- .element: style="text-align: right;" --> 

<p><!-- .element: style="text-align: center; color: #727272" -->
<small><small>6 novembre 2020</small></small>
</p>
---h---

## Your server as a function

- White paper (2013) de Marius Eriksen (Twitter)
- Définit des services avec 2 fonctions composables : 
  - **Service**<br/>
<small>`(Request) -> Future<Response>`</small>
  - **Filter**<br/>
<small>`(Request, Service) -> Future<Response>`</small>    


<div style="text-align: left">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i> Framework Finagle (Scala + Netty)</span>   
</div><!-- .element: class="fragment" data-fragment-index="1" -->

<div class="fragment" data-fragment-index="2">

- bons résultats mais... complexe : 
 - protocole agnostique, 
 - async,  
 - Scala peu populaire

</div>

---h---

<div style="text-align: center; border: 0; background: None; box-shadow: None;">
    <img src="img/http4k-bk.png" alt="logo" style="border: 0; background: None; box-shadow: None">
</div>
<div style="text-align: center; border: 0; background: None; box-shadow: None;">
    <img src="img/creators.png" alt="logo" style="border: 0; background: None; box-shadow: None">
</div>

<div style="text-align: left">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i> pur Kotlin</span>   
</div><!-- .element: class="fragment" data-fragment-index="1" -->
<div style="text-align: left">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i> aucune magie</span>   
</div><!-- .element: class="fragment" data-fragment-index="2" -->
<div style="text-align: left">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i> librairie légère</span>   
</div><!-- .element: class="fragment" data-fragment-index="3" -->
<div style="text-align: left">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i> simple et pragmatique</span>   
</div><!-- .element: class="fragment" data-fragment-index="4" -->
<div style="text-align: left">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i> tests faciles et rapides</span>   
</div><!-- .element: class="fragment" data-fragment-index="5" -->

Note:
- <small>Magie: proxy, reflexions, annotations</small>
- <small>Simple: que http, pas d'asynchrone</small>
- <small>Leger: Core juste kotlin-std</small>
---h---

## Concept : HttpMessage

> Un message sérialisé au standard HTTP

```kotlin
val request = Request(Method.GET, "http://server/path")
     .query("name", "value")
     .header("name", "value")
     .body("Hello world")
```
<!-- .element: class="fragment" data-fragment-index="1" -->

```kotlin
val response = Response(Status.I_M_A_TEAPOT)
     .header("name", "value")
     .body("Hello world")
```
<!-- .element: class="fragment" data-fragment-index="1" -->

---h---
## Concept : HttpHandler

> Transforme une requête HTTP en une réponse HTTP

<div style="text-align: center;">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i></span> <b>c'est une fonction !</b><!-- .element: class="fragment" data-fragment-index="1" -->
</div>

```kotlin
typealias HttpHandler = (Request) -> Response
```  
<!-- .element: class="fragment" data-fragment-index="1" -->


```kotlin
val echo: HttpHandler = { req: Request -> 
    Response(OK).body(req.body) 
}

val resp: Response = echo(Request(POST, "/echo").body("hello"))
```
<!-- .element: class="fragment" data-fragment-index="2" -->

Note:
- <small>Typealias: nom alternatifs aux types existants.</small>
---h---

## Concept : Filter

> Effectue un traitement avant ou après le traitement d'une operation HTTP

<div style="text-align: center;">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i></span> <b class="fragment" data-fragment-index="1">c'est une fonction !</b>
</div>


```
Filter: (HttpHandler) -> HttpHandler
```  
<!-- .element: class="fragment" data-fragment-index="1" -->

```kotlin
val twitterFilter: Filter = Filter { next: HttpHandler -> 
    { req: Request ->  
        val tweet: Request = req.body(req.bodyString().take(280)) 
        next(tweet) 
    } 
}
val tweet: HttpHandler = twitterFilter.then(handler)
```
<!-- .element: class="fragment" data-fragment-index="2" -->

---v---
## Filtres inclus

#### Filtres Serveur 
<small>Cors, Tracing, Authentification (Basique, Bearer, ApiKey), Gestion des exceptions,...</small>
#### Filtres Client 
<small>Tracing, Authentification, Cookies, Gzip,...</small>
#### Filtres Trafique 
<small>Réponse depuis un cache, enregistrement et rejeu</small>
#### Filtres Debug 
<small>Affichage des requests, ou des réponses ou des deux</small>
#### Filtres Requêtes et Réponses
<small>Compression, métriques, proxy, Base64</small> 

Note:
- <small>Tracing: ajout entêtes Zipkin</small>
- <small>Serveur autres: Copie des entêtes, Remplace la réponse avec un fichier statique</small>
- <small>CatchLensFailure: Convertit les erreurs de mapping de lens en BadRequest</small>
- <small>CatchAll: Convertit toutes les exception en InternalServerError</small>

---h---

## Concept : Router

>Fait correspondre un HttpHandler à une Requête

<div style="text-align: center;">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i></span> <b class="fragment" data-fragment-index="1">c'est une fonction !</b>
</div>

```
Router: (Request) -> HttpHandler?
```  
<!-- .element: class="fragment" data-fragment-index="1" -->

```kotlin
val routes: HttpHandler  = routes(
    "echo" bind POST to echo,
    "twitter" bind routes(
        "tweet" bind POST to tweet
    )
)
```
<!-- .element: class="fragment" data-fragment-index="2" -->

Note:
- <small>Routes composables</small>
- <small>Recherche Depth First Search sur l'arbre, 404 si pas trouvé</small>

---h---

## Passage de fonctions à serveur
<div class="fragment" data-fragment-index="1">

- On attache le HttpHandler à un serveur

```kotlin
val echo: HttpHandler = { r: Request -> Response(OK).body(r.body) }

val server: Http4kServer = echo.asServer(Jetty(8080)).start()
```
</div>
<div class="fragment" data-fragment-index="2">

- Support de :

| | |
|---|---|
| Jetty | Ktor |
| Netty | Apache
| Undertow | SunHttp | 
</div>

Note: 
- <small>Sun inclus dans le core, qu'en dev</small>

---h---

## Client HTTP

>Fait correspondre une Réponse HTTP à une Requête

<div style="text-align: center;">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i></span> <b class="fragment" data-fragment-index="1">c'est une fonction...</b> <span class="fragment" data-fragment-index="2">qu'on a déjà vue  : </span>
<b class="fragment" data-fragment-index="3">HTTPHandler ! </b>
</div>

```
HttpHandler: (Request) -> Response
```  
<!-- .element: class="fragment" data-fragment-index="3" -->

```kotlin
val client: HttpHandler = ApacheClient()
val response: Response = 
    client(Request(GET, "https://www.youtube.com/results")
         .query("search_query", "http4k"))
```
<!-- .element: class="fragment" data-fragment-index="4" -->

<div class="fragment" data-fragment-index="4">

- Support de :

| | |
|---|---|
| Apache | Jetty |
| OkHttp | Java |

</div>

Note: 
- <small>Java dans le Core</small>
- <small>Permet des tests simples sans connexion et la creation des services sans la nécessité d'un serveur http</small>
 
---h---

## Tests applications

Simples: 
- messages `data class` immutable
- tout est fonction

```kotlin
val echo: HttpHandler = { r: Request -> Response(OK).body(r.body) }

class EchoTest {
    @Test
    fun `handler echoes input`() {
        val input: Request = Request(POST, "/echo").body("hello")
        val expected: Response = Response(OK).body("hello")
        assetThat(echo(input), equalsTo(expectd))
    }
}
```
---v---
## Tests réutilisables à base de contracts (1/2)
#### Abstract test
```kotlin
abstract class TestContract {
    abstract val app : HttpHandler

    @Test
    fun `response body should be same a request`() {
        val expected: Response = Response(OK).body("hello")
        val input: Request = Request(Method.POST, "/echo").body("hello")
        assertThat(app(input), 
            hasStatus(expected.status) and hasBody(expected.bodyString()))
    }
}
```
---v---
## Tests réutilisables à base de contracts (2/2)
#### Test en mémoire
```kotlin
class TestInMemory : TestContract() {
    override val app: HttpHandler = { req: Request ->
        Response(OK).body(req.body)
    }
}
```
#### Test serveur
```kotlin
class TestAsServer : TestContract() {
    private val server = { req: Request ->
        Response(OK).body(req.body)
    }.asServer(Undertow(0)).start()

    override val app: HttpHandler = ClientFilters
        .SetBaseUriFrom(Uri.of("http://localhost:${server.port()}"))
        .then(OkHttp())
}
```
---v---
## Faux serveur validé par contrat (1/2)

```kotlin
data class Joke(val value: String)
class ChuckNorris(private val client: HttpHandler) {
    private val jsonBody = Body.auto<Joke>().toLens()
    fun randomJoke(): Joke {
        val response = client(Request(Method.GET, "https://api.chucknorris.io/jokes/random"))
        return jsonBody(response)
    }
}
```
```kotlin
abstract class ChuckNorrisCustomerContract {
    abstract val client: HttpHandler

    @Test
    fun getJoke() {
        val joke = ChuckNorris(client).randomJoke()
        joke.value shouldNot beEmpty()
    }
}
```

---v---
## Faux serveur validé par contrat (2/2)

```kotlin
class RealChuckTest : ChuckNorrisCustomerContract() {
    override val client: HttpHandler = OkHttp()
}
```
```kotlin
class FakeChuckTest : ChuckNorrisCustomerContract() {
    private val jsonBody = Body.auto<Joke>().toLens()
    override val client: HttpHandler = { _: Request ->
        jsonBody.inject(Joke("fake joke"), Response(Status.I_M_A_TEAPOT))
    }
}
```

---h---

## Contrats HTTP
```kotlin
val miner: HttpHandler = routes(
    "mine/{btc}" bind POST to { r: Request ->
        val newTotal: Int = r.path("btc")!!.toInt() + 1
        val out = "Vous avez $newTotal BTC"
        val json = """{ "value": "$out" }"""
        Response(OK).body(json)
    }
)
```
Renforcer les contrats des requêtes :
- Localisation : **Path**/**Query**/**Header**/**Body**/**Form**
- Requis, optionnel, valeur par défaut
- Type donnée
- Traitement du body avec vérification des types 

... et pour la réponse ... 

---v---

## Concept : Lens (lentille) 

> Un Lens cible une partie en particulier d'un objet complexe afin de récupérer ou définir une valeur

<div style="text-align: center;">
<span><i class="fas fa-arrow-right" style="color: #ff6600"></i></span> <b class="fragment" data-fragment-index="1">c'est une fonction...</b> <span class="fragment" data-fragment-index="2">plus précisément deux !</span>
</div>


```
Extract: (HttpMessage) -> X
Inject: (X, HttpMessage) -> HttpMessage
```  
<!-- .element: class="fragment" data-fragment-index="2" -->

<div class="fragment" data-fragment-index="5">

- **http4k** fournit des Lens pour les messages HTTP
- Le non respect <i class="fas fa-arrow-right" style="color: #ff6600"></i> Erreur Bad Request (HTTP 400)

</div>

Note: 
- <small>lens: abstraction qui zoom sur une partie</small>

---v---

## Exemple de Lens (1/6)

```kotlin
val btcPath = Path.int().of("btc")

val miner: HttpHandler = routes(
    "mine/{btc}" bind POST to { r: Request -> 
        val newTotal: Int = btcPath.extract(r) + 1
        val out = "Vous avez $newTotal BTC"
        val json = """{ "value": "$out" }"""
        Response(OK).body(json)
    }
)
```

---v---

## Exemple de Lens (2/6)

#### Mapping avec un type du domaine

```kotlin
data class BTC(val value: Int) {
   operator fun plus(that: BTC) = BTC(value + that.value)
   override fun toString() = value.toString()
}

val btcPath = Path.int().map(::BTC).of("btc")

val miner: HttpHandler = routes {
    "mine/{btc}" bind POST to { r: Request -> 
        val newTotal: BTC = btcPath.extract(r) + BTC(1)
        val out = "Vous avez $newTotal BTC"
        val json = """{ "value": "$out" }"""
        Response(OK).body(json)
    }
}
``` 

---v---

## Exemple de Lens (3/6)

#### Sérialisation automatique en JSON avec Jackson (ou Gson et Moshi)

```kotlin
data class BTC(val value: Int) {
   operator fun plus(that: BTC) = BTC(value + that.value)
   fun toString() = value.toString()
}
class MindedCoin(btc: BTC) {
    val value = "Vous avez ${btc + BTC(1)} BTC"
}

val btcPath = Path.int().map(::BTC).of("btc")
val jsonBody = Body.auto<MindedCoin>().toLens() 

val miner: HttpHandler = routes {
    "mine/{btc}" bind POST to { r: Request -> 
        val mined = MindedCoin(btcPath.extract(r))
        jsonBody.inject(mined, Response(OK))
    }
}
```

---v---

## Exemple de Lens (4/6)

#### Multiples utilisations (1/3) - Declaration

```kotlin
data class Child(val name: String)
data class Pageable(val sortAscending: Boolean, val page: Int, val maxResults: Int)

val nameHeader = Header.required("name")
val ageQuery = Query.int().optional("age")
val childrenBody = Body.string(TEXT_PLAIN)
                    .map(   { it.split(",").map(::Child) }, 
                            { it.joinToString { it.name } })
                    .toLens()
val pageable = Query.composite {
    Pageable(
        boolean().defaulted("sortAscending", true)(it),
        int().defaulted("page", 1)(it),
        int().defaulted("maxResults", 20)(it)
    )
}
``` 

---v---

## Exemple de Lens (5/6)

#### Multiples utilisations (2/3) - Utilisation serveur

```kotlin
val endpoint = { request: Request ->

    val name: String = nameHeader(request)
    val age: Int? = ageQuery(request)
    val children: List<Child> = childrenBody(request)
    val pagination = pageable(request)

    val msg = """
        $name is ${age ?: "unknown"} years old and has 
        ${children.size} children (${children.joinToString { it.name }})
        Pagination: $pagination
        """
    Response(OK).with(
        Body.string(TEXT_PLAIN).toLens() of msg
    )
}

val app = ServerFilters.CatchLensFailure.then(endpoint)
``` 
---v---

## Exemple de Lens (6/6)

#### Multiples utilisations (3/3) - Utilisation client

```kotlin
val goodRequest = Request(GET, "http://localhost:9000").with(
    nameHeader of "Jane Doe",
    ageQuery of 25,
    childrenBody of listOf(Child("Rita"), Child("Sue")))

val response = app(goodRequest)

``` 

---v---

## Lens pour les specifications OpenAPI

#### Definition route
```kotlin
fun mineBtcRoute(): ContractRoute {

    val btcPath = Path.int().map(::BTC).of("btc")
    val jsonBody = Body.auto<MindedCoin>().toLens() 
  
    val spec = "/mine" / btcPath meta {
        summary = "Mine a BTC"
        returning(OK, jsonBody to MindedCoin(5))
    } bindContract POST

    fun mine(btc: BTC): HttpHandler = { request: Request -> 
        val mined = MindedCoin(btc)
        Response(OK).with(jsonBody of mined)
    }
    return spec to ::mine 
}
``` 
---v---

## Lens pour les specifications OpenAPI

#### Definition service
```kotlin
val mySecurity = ApiKeySecurity(
                    Query.int().required("api"), 
                    { it == 42 })

val contract = contract {
    renderer = OpenApi3(ApiInfo("BTC Mine service", "v1.0"), Jackson)
    descriptionPath = "/swagger.json"
    security = mySecurity
    routes += mineBtcRoute()
}

val handler: HttpHandler = routes("/api" bind contract)

``` 
 
---v---

![Swagger](img/swagger.png)

---h---
## Des couches d'application

<img src="img/layers.png" alt="logo" style="border: 0; background: None; box-shadow: None">

---h---
## Integrations 1/2 

#### Serveurs 
Apache, Jetty, Netty, Ktor, SunHttp, Undertow

#### Clients
Apache, Java, Jetty, OkHttp, WebSockets

#### Moteurs de Templates 
Dust, Freemaker, Handlebars, Pebble, Thymeleaf, Jade4j

#### Formats messages
Json: **Jackson, Gson, Moshi**, KotlinX Serialization, Argo

XML, YAML

---v---
## Integrations 2/2
#### Autres

Oauth, Resilience (circuit breaker, rejeux, limite), Metrics, AWS, OpenApi, Serverless, WebDriver, Chaos, Approval Tests, Service Virtualisation
 
 ...

Note:
- <small>Resilience: support de circuits, rejeux, limite (integration avec Resilience4J)</small>
- <small>Metrics: avec micrometer</small>
- <small>WebDriver: WebDriver Selenium super leger et rapide car memoir</small>
- <small>Chaos: API pour déclarer et injecter des erreurs</small>
- <small>Virtualisation: enregistre de rejoue des versions de contrats HTTP</small>

---v---

## Performances 
<table>
<tr>
<td>
<h3>19ème au <a href="https://www.techempower.com/benchmarks/#section=data-r19&hw=ph&test=composite">Techempower</a></h3>
<br><small>(sur +100 frameworks, Spring 34ème)</small>
</td>
<td>
<img src="img/techempower.png" alt="Techempower">
</td>
</tr>
</table>

Note: 
- <small>Rapide car pas de reflexion, proxy ou dépendances.</small>

---h---
## Liens 

<i class="fas fa-code-branch" style="color: #ff6600"></i>&nbsp;&nbsp;&nbsp;Présentation : [razvfr.gitlab.io](https://razvfr.gitlab.io) |  Projet: [gitlab.com/razvfr](https://gitlab.com/razvfr)

------------
<i class="fas fa-info-circle" style="color: #ff6600"></i>&nbsp;&nbsp;&nbsp;[Documentation http4k.org](https://www.http4k.org/) 

<i class="fab fa-slack" style="color: #ff6600"></i>&nbsp;&nbsp;&nbsp;[Channel http4k/Slack Kotlin](https://kotlinlang.slack.com/archives/C5AL3AKUY) 

<i class="fas fa-file-pdf" style="color: #ff6600"></i>&nbsp;&nbsp;&nbsp;[Your Server as a Function](https://monkey.org/~marius/funsrv.pdf)

<i class="fas fa-bookmark" style="color: #ff6600"></i>&nbsp;&nbsp;&nbsp;[Hello Http4k](https://dmitrykandalov.com/hello-http4k)

<i class="fas fa-bookmark" style="color: #ff6600"></i>&nbsp;&nbsp;&nbsp;[Server as a function with Kotlin - http4k](https://kotlinexpertise.com/kotlin-http4k/)


------------
#### Vidéos KotlinConf
<i class="fab fa-youtube" style="color: red; padding-left: 1.5em;"></i>&nbsp;[2018 - Server as a Function in Kotlin](https://www.youtube.com/watch?v=p1VTfcQJefk)
<br/><i class="fab fa-youtube" style="color: red; padding-left: 1.5em;"></i>&nbsp;[2019 - Writing Test Driven Apps with http4k](https://www.youtube.com/watch?v=vdxBNh1qx1Q)

---h---
## Questions 

<br>
<br>
<p> <!-- .element: style="text-align: center;" -->
<i class="fas fa-question fa-3x" style="color: #ff6600"> </i>&nbsp;&nbsp;<i class="fas fa-question fa-3x" style="color: #000000"></i>&nbsp;&nbsp;<i class="fas fa-question fa-3x" style="color: #ff6600"></i> 
</p> 
<br>
<br>
<br>
<br>

---h---
## Merci 

<br>
<br>
<p> <!-- .element: style="text-align: center;" -->
<i class="fa fa-thumbs-up fa-5x" aria-hidden="true" style="color: #ff6600"></i>
</p>
<br>
<br>

<br>
<br>
