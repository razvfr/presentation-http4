package fr.rzv

import org.http4k.contract.*
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.contract.security.ApiKeySecurity
import org.http4k.core.*
import org.http4k.filter.DebuggingFilters
import org.http4k.filter.ServerFilters
import org.http4k.format.Jackson
import org.http4k.format.Jackson.auto
import org.http4k.lens.Path
import org.http4k.lens.Query
import org.http4k.lens.int
import org.http4k.routing.ResourceLoader
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.routing.static
import org.http4k.server.Netty
import org.http4k.server.Undertow
import org.http4k.server.asServer
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage


data class BTC(val value: Int) {
    operator fun plus(that: BTC) = BTC(value + that.value)
    override fun toString() = value.toString()
}

class MindedCoin(btc: BTC) {
    val value = "Vous avez ${btc + BTC(1)} BTC"
}

fun mineBtcRoute(): ContractRoute {

    val btcPath = Path.int().map(::BTC).of("btc")
    val jsonBody = Body.auto<MindedCoin>().toLens()

    val spec = "mine" / btcPath meta {
        summary = "Mine a BTC"
        returning(Status.OK, jsonBody to MindedCoin(BTC(5)))
    } bindContract Method.POST

    fun mine(btc: BTC): HttpHandler = { _: Request ->
        val mined = MindedCoin(btc)
        Response(Status.OK).with(jsonBody of mined)
    }
    return spec to ::mine
}

fun main() {
    val mySecurity = ApiKeySecurity(Query.int().required("api"), { it == 42 })

    val contract = contract {
        renderer = OpenApi3(ApiInfo("BTC Mine service", "v1.0"), Jackson)
        descriptionPath = "/swagger.json"
        security = mySecurity
        routes += mineBtcRoute()
    }

    val handler: HttpHandler = routes(
        "/contract/api/v1" bind DebuggingFilters.PrintRequestAndResponse().then(contract),
        "/swagger" bind static(ResourceLoader.Classpath("swagger")),
        "/terminal" bind static(ResourceLoader.Classpath("terminal")),
        "/" bind static(ResourceLoader.Classpath("slides"),
            "md" to ContentType.TEXT_PLAIN,
            "woff2" to ContentType("application/x-font-woff2"),
            "ttf" to ContentType("application/x-font-ttf"),
            "ico" to ContentType("image/x-icon")),
    )

    websocketTerminal()

    ServerFilters.CatchAll()
        .then(handler)
        .asServer(Undertow(8080)).start()
}

fun WsMessage.bodyStrings() = with(this.bodyString()) {
    if (this.startsWith("rm ") || this.contains(" rm ")) "echo Vous y avez cru ? \uD83D\uDE00" else this
}

fun websocketTerminal() =
    { ws: Websocket ->
        ws.onMessage {
            ws.send(
                WsMessage(
                    Runtime.getRuntime()
                        .exec(it.bodyStrings())
                        .inputStream.reader().readText()
                )
            )
        }
    }.asServer(Netty(8000)).start()
